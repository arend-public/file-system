import sys

if __name__ == '__main__':
    from cli.main import main as _main
    sys.exit(_main())
