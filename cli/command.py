import os

from cli.input import Input

ROOT_DIRS = [
    'bin',
    'dev',
    'etc',
    'home',
    'lib',
    'media',
    'mnt',
    'opt',
    'proc',
    'root',
    'run',
    'sbin',
    'srv',
    'sys',
    'tmp',
    'usr',
    'var',
]


def traverse_dir(dir_name: str):
    if dir_name == '/':
        dirs = [os.path.join(dir_name, x) for x in ROOT_DIRS]
        files = []
    else:
        try:
            listings = [os.path.join(dir_name, x) for x in os.listdir(dir_name) if
                        not os.path.islink(os.path.join(dir_name, x)) and x.islower() and x.isalpha()]
        except OSError:
            listings = []
        dirs = sorted([x for x in listings if os.path.isdir(x)])
        files = sorted([x for x in listings if os.path.isfile(x)])
    for d in dirs:
        yield from traverse_dir(d)
    for f in files:
        yield f


def run(_input: Input) -> int:
    result_count = 0
    for f in traverse_dir(_input.start_dir):
        print(f)
        result_count += 1
        if _input.limit == -1:
            continue
        if result_count >= _input.limit:
            break

    return 0
