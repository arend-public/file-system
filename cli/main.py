from cli.input import parse
from cli.command import run


def main() -> int:
    try:
        return run(parse())
    except BaseException as error:
        print(error)
        return 1
