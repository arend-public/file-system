from __future__ import annotations

import os.path
from sys import argv
from typing import TypeVar


class Input:
    def __init__(self, args):
        self.args = args
        self.argc = len(args)
        if self.argc >= 2:
            self._start_dir = self.args[1]
        self.limit = self.find_option_value(['-l', '--limit'], int, -1)

    @property
    def start_dir(self):
        return os.path.realpath(self._start_dir)

    def find_option_index(self, option_names: list[str]) -> int | None:
        cnt = 0
        for a in self.args:
            if a in option_names:
                return cnt
            cnt += 1
        return None

    def find_option_value(self, option_names: list[str], t: type = str, default_value: any = None) -> any:
        idx = self.find_option_index(option_names)
        if not idx or idx + 1 >= self.argc:
            return t(default_value)
        try:
            return t(self.args[idx + 1])
        except (IndexError, ValueError):
            return t(default_value)


def parse() -> Input:
    return Input(argv)
